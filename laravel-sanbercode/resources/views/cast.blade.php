@extends('layouts.main');


@section('main')
<div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="modal" tabindex="-1" role="dialog" id="modalPage">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="page">
  
                </div>
              </div>
              <div class="modal-footer">
                <button id="save" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <h3 class="card-title">DataTable with minimal features & hover style</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <button id="add" class="btn btn-primary">Tambah</button>
        <table id="example2" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($casts as $key => $cast)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $cast->nama }}</td>
            <td>{{ $cast->umur }}</td>
            <td>{{ $cast->bio }}</td>
            <td>
              <button onclick="show({{ $cast->id }})" class="btn btn-primary">Show</button>
              <button  onclick="edit({{ $cast->id }})" class="btn btn-warning">Edit</button>
              <form method="post" id="hapusUser" class="d-inline">
                @method('DELETE')
                @csrf
             </form>
              <button onclick="destroy({{ $cast->id }})" class="btn btn-danger">Hapus</button>
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $('#add').click(function(){
        $.ajax({
            url : `{{ url('cast/create') }}`,
            method: 'GET',
            success: function(data){
                $('#modalPage').modal('show'); 
                $('.page').html(data);
                $('#save').click(function(){
                  $.ajax({
                    url: `{{ url('cast') }}`,
                    method: "POST",
                    data: $('#formAdd').serialize(),
                    success: function(data){
                      alert(data);
                      location.reload();
                    },
                    error: function(data){
                      console.log(data);
                    }
                  })
                })
            }
        });
         
    });


    function destroy(id)
    {
      $.ajax({
        url: `{{ url('destroy') }}/` + id,
        method: "post",
        data: $('#hapusUser').serialize(),
        success: function(data){
          alert(data);
          location.reload();
        },
        error: function(data){
          console.log(data);
        }

      })
    }


    function show(id){
      $.ajax({
            url : `{{ url('cast') }}/` + id,
            method: 'GET',
            success: function(data){
                $('#modalPage').modal('show'); 
                $('.page').html(data);
                $('#save').remove();
            },
            error: function(data){
              console.log(data);
            }
        });
    }

    function edit(id){
      $.ajax({
            url : `cast/`+id+`/edit`,
            method: 'GET',
            success: function(data){
                $('#modalPage').modal('show'); 
                $('.page').html(data);
                $('#save').click(function(){
                  $.ajax({
                        url: `{{ url('cast') }}` + id,
                        method: "POST",
                        data: $('#formEdit').serialize(),
                        success: function(data){
                          alert(data);
                          location.reload();
                        },
                        error: function(data){
                          console.log(data);
                        }
                      });
                  });

                  
            },
            error: function(data){
              console.log(data);
            }
        });
    }
   
</script>
@endsection


