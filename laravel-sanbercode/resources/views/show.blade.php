<form>
    @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" name="nama" class="form-control" id="nama" value="{{ $cast->nama }}" readonly>
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" name="umur" class="form-control" id="umur"  value="{{ $cast->umur }}" readonly>
        </div>
        <div class="form-group">
          <label for="bio">BIO</label>
          {{-- <input type="password" class="form-control" id="bio" placeholder="Password"> --}}
          <textarea name="bio" id="bio" cols="50" rows="5" readonly>{{ $cast->bio }}</textarea>
        </div>
      <!-- /.card-body -->    
    </form>