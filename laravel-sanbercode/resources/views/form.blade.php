<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
    <h3>Buat Account Baru!</h3>
    <h2>Sign Up Form</h2>
    <form action="{{ route("auth.create") }}" method="post">
        @csrf
        <label for="firstname">First name : </label>
        <br><br>
        <input type="text" name="firstname" id="firstname">
        <br><br>
        <label for="lastname">Last name : </label>
        <br><br>
        <input type="text" name="lastName" id="firstname">
        <br><br>
        <label for="gender">Gender: </label><br><br>
        <input type="radio" name="gender" value="male" id="gender">Male<br>
        <input type="radio" name="gender" value="female" id="gender">Female<br>
        <input type="radio" name="gender" value="other" id="gender">Other<br>
        <br><br>
        <label for="nationality">Nationality : </label>
        <br><br>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singapore">Singapore</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>
        <br><br>
        <label for="spoken">Language Spoken : </label><br><br>
        <input type="checkbox" value="indonesia" name="spoken" id="spoken">Bahasa Indonesia<br>
        <input type="checkbox" value="english" name="spoken" id="spoken">English<br>
        <input type="checkbox" value="other" name="spoken" id="spoken">Other<br>
        <br><br>
        <label for="bio">Bio : </label><br>
        <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br>
<input type="submit" value="signup">
    </form>
</body>
</html>