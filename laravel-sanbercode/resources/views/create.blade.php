<form action="" method="POST" id="formAdd">
  @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama">
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" name="umur" class="form-control" id="umur" placeholder="Umur">
      </div>
      <div class="form-group">
        <label for="bio">BIO</label>
        {{-- <input type="password" class="form-control" id="bio" placeholder="Password"> --}}
        <textarea name="bio" id="bio" cols="50" rows="5"></textarea>
      </div>
    <!-- /.card-body -->    
  </form>