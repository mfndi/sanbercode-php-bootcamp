<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup()
    {
        return view('form');
    }

    public function create(Request $request)
    {
    
        $namaLengkap = $request->firstname ." ". $request->lastName;
        return view('welcome', [
            'nama' => $namaLengkap
        ]);
    }
}
