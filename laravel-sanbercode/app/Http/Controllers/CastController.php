<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(Cast $cast)
    {
        return view('cast', [
            'casts' => $cast::get()
        ]);
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'bio' => ['required'],
            'umur' => ['required']
        ]);

        DB::table('cast')->insert([
            'nama' => $request->nama,
            'bio' => $request->bio,
            'umur' => $request->umur
        ]);

        return response()->json("Berhasil simpan");
    }



    public function show(Request $request){
        $cast = DB::table('cast')->where('id', $request->id)->first();
        return view('show',[
            'cast' => $cast
        ]);
    }


    public function edit(Request $request)
    {
        $cast = DB::table('cast')->where('id', $request->id)->first();
        return view('edit',[
            'cast' => $cast
        ]);
    }



    public function update(Request $request)
    {
        $request->validate([
            'nama' => ['required'],
            'bio' => ['required'],
            'umur' => ['required']
        ]);

        DB::table('cast')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'bio' => $request->bio,
            'umur' => $request->umur
        ]);

        return response()->json("Berhasil edit");
    }




























    public function destroy(Request $request)
    {
        // ddd($request->id);
        DB::table('cast')->where('id', $request->id)->delete();
        return \response()->json('Berhasil Hapus User');
    }
}
