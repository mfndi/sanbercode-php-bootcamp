<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'signup'])->name('auth.signup');
Route::post('/welcome',  [AuthController::class, 'create'])->name('auth.create');



Route::get('/table', function(){
    return view('table');
});


Route::get('/data-tables', function(){
    return view('data-tables');
});


Route::get('/cast', [CastController::class, 'index']);
Route::get('cast/create', [CastController::class, 'create']);
Route::POST('/cast', [CastController::class, 'store']);
Route::get('cast/{id}', [CastController::class, 'show']);
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::PUT('cast/{id}', [CastController::class, 'update']);




Route::DELETE('/destroy/{id}', [CastController::class, 'destroy']);